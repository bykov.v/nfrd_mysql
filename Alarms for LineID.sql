#set @param = 'DEU.DTAG.HWTOP';
set @param = 'DEU.DTAG.HWTVX';

select
    rdservice.lineid0 as LineID,
    
    case when 
     (rdauthfail.status0 is null) and (rdconfigfail.status0 is null) and (rdunavail.status0 is null)
     then null 
     else concat(ifnull(rdauthfail.eventtype0,'-'), "/", ifnull(rdconfigfail.eventtype0, '-'), "/", ifnull(rdunavail.eventtype0,'-')) end 
    AS alarms,

    rdservice.rdservicetype as service,
    rdservice.status as service_status

    
    #ip_address.ip as ip,
    
from netfactoryrd.rdservice_v02 as rdservice
#join netfactoryrd.generalserviceattributes as gsa on rdservice.rfsid0 = gsa.parentid and gsa.servicename != 'Business Ethernet Bridge' and gsa.servicename != 'Business RD Port'


#alarms
left join netfactoryrd.alarmstatus as rdauthfail on rdauthfail.rfsid0 = rdservice.rfsid0 and rdauthfail.status0 != 'cleared' and rdauthfail.eventtype = "rdauthfail"
left join netfactoryrd.alarmstatus as rdconfigfail on rdconfigfail.rfsid0 = rdservice.rfsid0 and rdconfigfail.status0 != 'cleared' and rdconfigfail.eventtype = "rdconfigfail"
left join netfactoryrd.alarmstatus as rdunavail on rdunavail.rfsid0 = rdservice.rfsid0 and rdunavail.status0 != 'cleared' and rdunavail.eventtype = "rdunavail"

where rdservice.lineid0 like @param or rdservice.subscriptionid0 like @param

set @lineid = 'DEU.DTAG.HVD73';

select 
 rdservice.lineid0 as lineid,
 gsa.parentid,
 gsa.servicename,
 gsa.uniqueid,
 beb.evc_num
-- bp.parentuid as BP_id
from netfactoryrd.rdservice_v02 as rdservice
 join netfactoryrd.generalserviceattributes as gsa on rdservice.rfsid0 = gsa.parentid and gsa.servicename like '%Business RD Port%'
 inner join (select nirfsid, count(nirfsid) as evc_num from netfactoryrd.businessethernetbridge_v03 group by nirfsid) as beb on gsa.uniqueid = beb.nirfsid
#where rdservice.lineid0 like 'DEU.DTAG.HVD73';
where rdservice.lineid0 = @lineid
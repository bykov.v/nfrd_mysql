SET @mac = replace("00:a0:c8:e8:5b:19", ":", "");
#set @new_mac = replace(@mac, ":","");
#select @mac;
select
 concat(device.vendor0, " ", device.cliidentifier0) as device,
 device.serialnumber0,
 fw_version.firmwareversion as fw_version,
 device.baseconfigurationversion as device_gk,
 ssh_pass.password as telekom_password
from
 netfactoryrd.device as device
 left join netfactoryrd.firmwareversion as fw_version on fw_version.parentuid = device.id
 left join netfactoryrd.physicalport as phy_port on phy_port.physicalportmacaddress = device.macaddress
 left join netfactoryrd.sshuseraccount as ssh_pass on ssh_pass.unid = phy_port.parentuid and ssh_pass.username = 'telekom'
where
 device.macaddress like @mac or device.macaddress0 like @mac;
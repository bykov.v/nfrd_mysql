#set @line_id = 'DEU.DTAG.HWQ8B';
set @line_id = 'DEU.DTAG.HWPZD';
#set @line_id = 'DEU.DTAG.HWQ8B';

select
 bb_ep1.evcid as evc_id,
 bb_ep1.qosprofilel2 as evc_qos,
 bb_ep1.bandwidthingress as evc_speed,
 gsa_ep1.groupid as subscription_id,
 
 
 rds_ep1.lineid0 as ep_1,
 CASE
  when (rdauthfail_ep1.status0 is null) and (rdconfigfail_ep1.status0 is null) and (rdunavail_ep1.status0 is null)
  then null 
  else concat(ifnull(rdauthfail_ep1.eventtype0,'-'), "/", ifnull(rdconfigfail_ep1.eventtype0, '-'), "/", ifnull(rdunavail_ep1.eventtype0,'-')) END
 AS alarms_ep1,
 #concat(rdauthfail_ep1.eventtype0, '/', rdconfigfail_ep1.eventtype0, '/', rdunavail_ep1.eventtype0) as alarms_ep1,
 rds_ep1.status as status_ep1,
 gsa_ep1.status as evc_status_ep1,
 #gsa_ep1.datecreated as created,
 #device_ep1.serialnumber0 as ep1_sn,
 #ssh_pass_ep1.password as telekom_pw_ep1,
 
 bp_ep1.networkinterfaceid as uni_id_ep1,
 bb_ep1.ttag as t_tag_ep1,
 CASE WHEN bb_ep1.stag = 0 THEN NULL ELSE bb_ep1.stag END AS s_tag_ep1,
 CASE WHEN vlanrange_ep1.tagstart = 0 THEN NULL ELSE vlanrange_ep1.tagstart END AS ce_vlan_start_ep1, 
 CASE WHEN vlanrange_ep1.tagend = 0 THEN NULL ELSE vlanrange_ep1.tagend END AS ce_vlan_end_ep1,
 #bb_ep1.cosmappingenable as cos_mapping_ep1,
 #bb_ep1.isactiveendpoint as active_ep1,

 rds_ep2.lineid0 as lineid_ep2,
 CASE
  when (rdauthfail_ep2.status0 is null) and (rdconfigfail_ep2.status0 is null) and (rdunavail_ep2.status0 is null)
  then null 
  else concat(ifnull(rdauthfail_ep2.eventtype0,'-'), "/", ifnull(rdconfigfail_ep2.eventtype0, '-'), "/", ifnull(rdunavail_ep2.eventtype0,'-')) end 
 AS alarms_ep2, 

 rds_ep2.status as status_ep2,
 gsa_ep2.status as evc_status_ep2,
 #gsa_ep2.datecreated as created,
 #device_ep2.serialnumber0 as ep2_sn,
 #ssh_pass_ep2.password as telekom_pw_ep2,
 
 bp_ep2.networkinterfaceid as uni_id_ep2,
 bb_ep2.ttag as t_tag_ep2,
 CASE WHEN bb_ep2.stag = 0 THEN NULL ELSE bb_ep2.stag END AS s_tag_ep2,
 CASE WHEN vlanrange_ep2.tagstart = 0 THEN NULL ELSE vlanrange_ep2.tagstart END AS ce_vlan_start_ep2, 
 CASE WHEN vlanrange_ep2.tagend = 0 THEN NULL ELSE vlanrange_ep2.tagend END AS ce_vlan_end_ep2
 #bb_ep2.cosmappingenable as cos_mapping_ep2,
 #bb_ep2.isactiveendpoint as active_ep2

from netfactoryrd.rdservice_v02 as rds_ep1
 #endpoint1
 left join netfactoryrd.generalserviceattributes as gsa_ep1 on gsa_ep1.parentid = rds_ep1.rfsid0 and gsa_ep1.servicename = "Business Ethernet Bridge"
 left join netfactoryrd.businessethernetbridge_v03 as bb_ep1 on bb_ep1.parentuid = gsa_ep1.uniqueid 
 left join netfactoryrd.vlantagrange_v03 as vlanrange_ep1 on vlanrange_ep1.parentuid = bb_ep1.parentuid
 left join netfactoryrd.businessrdport_v03 as bp_ep1 on bp_ep1.parentuid = bb_ep1.nirfsid
 #device_info
 left join netfactoryrd.featureset as featureset_ep1 on featureset_ep1.rfsid = gsa_ep1.parentid
 left join netfactoryrd.sshuseraccount as ssh_pass_ep1 on ssh_pass_ep1.parentuid = featureset_ep1.deviceid and ssh_pass_ep1.username = 'telekom'
 left join netfactoryrd.device as device_ep1 on device_ep1.id = featureset_ep1.deviceid 
 #device alarms:
 left join netfactoryrd.alarmstatus as rdauthfail_ep1 on rdauthfail_ep1.objectidentifier = gsa_ep1.parentid and rdauthfail_ep1.status0 != 'cleared' and rdauthfail_ep1.eventtype0 = "rdauthfail"
 left join netfactoryrd.alarmstatus as rdconfigfail_ep1 on rdconfigfail_ep1.objectidentifier = gsa_ep1.parentid and rdconfigfail_ep1.status0 != 'cleared' and rdconfigfail_ep1.eventtype0 = "rdconfigfail"
 left join netfactoryrd.alarmstatus as rdunavail_ep1 on rdunavail_ep1.objectidentifier = gsa_ep1.parentid and rdunavail_ep1.status0 != 'cleared' and rdunavail_ep1.eventtype0 = "rdunavail"

 
 #endpoint2:
 left join netfactoryrd.businessethernetbridge_v03 as bb_ep2 on bb_ep2.subscriptionid = bb_ep1.subscriptionid and bb_ep2.parentuid != bb_ep1.parentuid
 left join netfactoryrd.generalserviceattributes as gsa_ep2 on gsa_ep2.uniqueid = bb_ep2.parentuid 
 left join netfactoryrd.rdservice_v02 as rds_ep2 on rds_ep2.rfsid0 =gsa_ep2.parentid
 left join netfactoryrd.vlantagrange_v03 as vlanrange_ep2 on vlanrange_ep2.parentuid = bb_ep2.parentuid
 left join netfactoryrd.businessrdport_v03 as bp_ep2 on bp_ep2.parentuid = bb_ep2.nirfsid
 #device_ep2 info
 left join netfactoryrd.featureset as featureset_ep2 on featureset_ep2.rfsid = gsa_ep2.parentid
 left join netfactoryrd.sshuseraccount as ssh_pass_ep2 on ssh_pass_ep2.parentuid = featureset_ep2.deviceid and ssh_pass_ep2.username = 'telekom'
 left join netfactoryrd.device as device_ep2 on device_ep2.id = featureset_ep2.deviceid 
 #device_ep2 alarms:
 left join netfactoryrd.alarmstatus as rdauthfail_ep2 on rdauthfail_ep2.objectidentifier = gsa_ep2.parentid and rdauthfail_ep2.status0 != 'cleared' and rdauthfail_ep2.eventtype0 = "rdauthfail"
 left join netfactoryrd.alarmstatus as rdconfigfail_ep2 on rdconfigfail_ep2.objectidentifier = gsa_ep2.parentid and rdconfigfail_ep2.status0 != 'cleared' and rdconfigfail_ep2.eventtype0 = "rdconfigfail"
 left join netfactoryrd.alarmstatus as rdunavail_ep2 on rdunavail_ep2.objectidentifier = gsa_ep2.parentid and rdunavail_ep2.status0 != 'cleared' and rdunavail_ep2.eventtype0 = "rdunavail"
 
where rds_ep1.lineid0 like @line_id  and gsa_ep1.status = "provisioned"
order by evc_qos#t_tag_ep1
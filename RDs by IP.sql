SET @ip = "172.22.28.%";
#set @new_mac = replace(@mac, ":","");
#select @mac;
select
 lease.ip as ip,
 lease.dhcpmacaddress as mac,
 concat(device.vendor0, " ", device.cliidentifier0) as device,
 device.serialnumber0 as sn,
 fw_version.firmwareversion as fw_version,
 device.baseconfigurationversion as device_gk,
 lease.startupacs as acs_sever,
 lease.dhcpclusternode as dhcp_cluster,
 device.uniqueidentifier as device_id,
 lease.endtimestring as end_time,
 ssh_pass.password as telekom_password
from
 netfactoryrd.view_device_leaseinfo as lease
 left join netfactoryrd.device as device on ( device.macaddress = lease.dhcpmacaddress or device.macaddress0 = lease.dhcpmacaddress)
 left join netfactoryrd.firmwareversion as fw_version on fw_version.parentuid = device.id
 left join netfactoryrd.physicalport as phy_port on phy_port.physicalportmacaddress = device.macaddress
 left join netfactoryrd.sshuseraccount as ssh_pass on ssh_pass.unid = phy_port.parentuid and ssh_pass.username = 'telekom'
where
 lease.ip like @ip;
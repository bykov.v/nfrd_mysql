SELECT 
 distinct servicename
FROM netfactoryrd.generalserviceattributes x;


#get all modules for the device by name
select 
 unis.uni_name,
 md.telekomid
from (select 
 distinct reference as uni_name
from 
 netfactoryrd.modulerequirement1 as mdr where mdr.parentuid like '%HUA1G%') as unis

join netfactoryrd.moduledefinition as md on md.idstring = unis.uni_name;
 

set @uni_rfs_id = "1591009205456-1635";

select 
 beb.evcid as evc_id,
 beb.ttag as t_tag,
 beb.stag as s_tag,
 service_attributes.status as status,
 service_attributes.datecreated as created
from 
   netfactoryrd.businessethernetbridge_v03 as beb
   inner join netfactoryrd.generalserviceattributes as service_attributes on service_attributes.uniqueid = beb.parentuid  and service_attributes.status = "provisioned"
where 
 beb.nirfsid = @uni_rfs_id
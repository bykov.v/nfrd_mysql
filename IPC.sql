#set @subscription_id = '000000001443291';
#set @subscription_id = '000000001454121';
set @sybscription_id = '000000001454009';

select 
	rdservice.lineid0 as line_id,
	rdservice.rfsid0 as rfs_id,
	rdservice.subscriptionid0 as subscription_id,
	rdservice.status as status,
	device.serialnumber0 as device_sn,
	device.baseconfigurationversion as device_gk,
	concat(device.vendor0, " ", device.cliidentifier0) as device,
	ip_address.ip as ip,
	ssh_pass.password as telekom_password,
	fw_version.firmwareversion as device_fw,
	service.anschlussvariante as rd_role,
	service.montageart as montageart,
	service.t4portaktiv as t4_active,
	service.dienstebandbreiteup as access_speed_up,
	service.dienstebandbreitedown as access_speed_down,
	snmp.aktiviert as snmpv3_status,
	concat(snmpaddress.prefix, '/', snmpaddress.prefixlaenge) as snmp_address,
	rdserviceparam.type as servive_type,
	rdserviceparam.bandbreite_service as service_speed,
	rdserviceparam.qos_profil as qos,
	rdserviceparam.ttag as t_tag,
	concat(ports.networkinterfacetype, ports.portnumber) as uni_num,
	ports.physicallayer as uni_type,
	ports.networkinterfaceid as uni_id,
	ports.bundlingtype as uni_bundling,
	concat(vrrpipv4.ipaddress, '/', vrrpipv4.prefixlaenge) as vrrp_ipv4,
	concat(kundenipv4.ipaddress, '/', kundenipv4.prefixlaenge) as kunden_ipv4,
	concat(staticroutev4.prefix, '/', staticroutev4.prefixlaenge) as static_route_v4,	
	concat(vrrpipv6.ipaddress, '/', vrrpipv6.prefixlaenge) as vrrp_ipv6,
	concat(kundenipv6.ipaddress, '/', kundenipv6.prefixlaenge) as kunden_ipv6,
	concat(staticroutev6.prefix, '/', staticroutev6.prefixlaenge) as static_route_v6,
	#unidata.bandwidth as uni_bw,
	concat(ip_address_transit.ipaddresstransitipv4rd, '/',ip_address_transit.prefixlaenge) as ip_address_transit_rd_v4,
	concat(ip_address_transit.ipaddresstransitipv4bng, '/',ip_address_transit.prefixlaenge) as ip_address_trait_bng_v4,
	rdserviceparam.ipaddressrdloopbackipv4 as loopback_ipv4,
	rdserviceparam.ipaddressrdloopbackipv6 as loopback_ipv6,
	rdserviceparam.enablevrrp as vrrp,
	case when rdserviceparam.enablevrrp = 1 then rdserviceparam.redundanzmodus end as vrrp_mode,
	case 
	 when rdserviceparam.enablevrrp = 1 and rdserviceparam.rolle = "Primary" then "Master" 
	 when rdserviceparam.enablevrrp = 1 and rdserviceparam.rolle = "Secondary" then "Slave"
	 else rdserviceparam.rolle
	end as vrrp_role,
	case when rdserviceparam.enablevrrp = 1 then rdserviceparam.vrrpgroupidipv4 end as vrrp_group_v4,
	case when rdserviceparam.enablevrrp = 1 then rdserviceparam.vrrpgroupidipv6 end as vrrp_group_v6
from 
    netfactoryrd.basicservice as bs
	left join netfactoryrd.rdservice_v02 as rdservice on rdservice.rfsid0 = bs.parentrfsid 
	left join netfactoryrd.rfs_business_rd_mgmt1_v03 as service on service.parentuid = rdservice.rfsid0
	left join netfactoryrd.rfs_business_ip_gateway1_v03 as rdserviceparam on rdserviceparam.unid like concat('%', bs.rfsid ,'%')
	#left join netfactoryrd.port_conflet_v02 as unidata on (unidata.portlocator like '%UNI%') and (unidata.parentuid like concat('%', rdservice.rfsid0, '%'))
	left join netfactoryrd.aclipv4_dest_conflet_v02 as aclipv4 on aclipv4.parentuid = rdservice.rfsid0
	left join netfactoryrd.rfsipaddresstransit1_v03 as ip_address_transit on ip_address_transit.parentuid like concat('%', bs.rfsid,'%')
	left join netfactoryrd.businessrdport_v03 as ports on ports.parentuid = rdserviceparam.nirfsid 
	
	#ip address:
	left join netfactoryrd.rfsipaddress4_v03 as vrrpipv4 on vrrpipv4.parentuid like concat('%', bs.rfsid,'%')
	left join netfactoryrd.rfsipaddress5_v03 as kundenipv4 on kundenipv4.parentuid like concat('%', bs.rfsid,'%')
   	left join netfactoryrd.rfsipaddress6_v03 as vrrpipv6 on vrrpipv6.parentuid like concat('%', bs.rfsid,'%')
	left join netfactoryrd.rfsipaddress7_v03 as kundenipv6 on kundenipv6.parentuid like concat('%', bs.rfsid,'%')
	#static route:
	left join netfactoryrd.rfsstatischeroute2_v03 as staticroutev4 on staticroutev4.parentuid like concat('%', bs.rfsid,'%')
	left join netfactoryrd.rfsstatischeroute3_v03 as staticroutev6 on staticroutev6.parentuid like concat('%', bs.rfsid,'%')
	#snmp
	left join netfactoryrd.rfssnmpv33 as snmp on snmp.parentuid like concat('%', rdservice.rfsid0,'%')
	left join netfactoryrd.rfsacl6 as snmpaddress on snmpaddress.parentuid like concat('%', rdservice.rfsid0,'%')
	#device
	left join netfactoryrd.featureset as featureset on featureset.rfsid = rdservice.rfsid0
	left join netfactoryrd.device as device on device.uniqueidentifier = featureset.deviceid 
	left join netfactoryrd.firmwareversion as fw_version on fw_version.parentuid = featureset.deviceid
	left join netfactoryrd.physicalport as phy_port on phy_port.physicalportmacaddress = device.macaddress
    left join netfactoryrd.sshuseraccount as ssh_pass on ssh_pass.unid = phy_port.parentuid and ssh_pass.username = 'telekom'
    #ip address
    left join netfactoryrd.view_device_leaseinfo as ip_address on ip_address.dhcpmacaddress = device.macaddress or ip_address.dhcpmacaddress = device.macaddress0

where (bs.subscriptionid like @subscription_id)
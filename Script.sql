select
 case 
  when device.serialnumber0 is not NULL 
  then device.serialnumber0 
 end as sn,
 
 ports.networkinterfaceid as uni_id,
 ip_addresses.ip as ip_addr
from 
 netfactoryrd.endgeraetefeatures_line1_v03 as geradefeatures
 left join netfactoryrd.featureset as featureset on featureset.rfsid = geradefeatures.unid 
 inner join netfactoryrd.device as device on (device.uniqueidentifier = featureset.deviceid) and (device.serialnumber0 is not NULL)
 left join netfactoryrd.port_conflet_v02 as ports on (ports.enableclock = 1) and (ports.networkinterfaceid is not null) and (ports.parentuid like concat('%', featureset.rfsid, '%'))
 left join netfactoryrd.view_device_leaseinfo as ip_addresses on (ip_addresses.dhcpmacaddress like device.macaddress0)  or (ip_addresses.dhcpmacaddress like device.macaddress0) 
 #left join netfactoryrd.businessrdport_v03 as bport on bport.parentuid = featureset.rfsid 
where 
 geradefeatures.uebertragungsprotokoll like "%VDSL%"
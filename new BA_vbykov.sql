set @lineid = 'DEU.DTAG.HWONM';


select 
  rds.rfsid0 as rfsID,
  rds.status as status,
  concat (brp.networkinterfacetype, brp.portnumber) as interface,
  brp.networkinterfaceid as uni_id,
  brp.ethertype as ethertype,
  rdmgmt.lineid as lineID,
  
  rdmgmt.bandbreite_anschluss as bb,
  rdmgmt.anschlussvariante as anschlussvariante,
  rdmgmt.montageart as monageart,
  rdmgmt.stromversorgung as strom
  
from 
	netfactoryrd.rdservice_v02 as rds
	inner join netfactoryrd.rfs_business_rd_mgmt1_v03 as rdmgmt on rdmgmt.parentuid = rds.rfsid0 
	inner join netfactoryrd.generalserviceattributes as gsa on gsa.parentid = rds.rfsid0 and gsa.servicename = "Business RD Port"
	inner join netfactoryrd.businessrdport_v03 as brp on brp.parentuid = gsa.uniqueid 
where rds.lineid0 = @lineid

set @subscription_id = 'DEU.DTAG.HVMWO';

select  
    rdservice.rdservicetype as service,
	rdprofile.status0 as profile_status, 
	rdservice.status as service_status, 
	rdauthfail.status0 as rdauthfail_status, 
	rdauthfail.eventtype0 as rdauthfail_type, 
	rdauthfail.messagetype0 as rdauthfail_message, 
	rdconfigfail.status0 as rdconfigfail_status, 
	rdconfigfail.eventtype0 as rdconfigfail_type, 
	rdconfigfail.messagetype0 as rdconfigfail_message, 
	rdunavail.status0 as rdunavail_status, 
	rdunavail.eventtype0 as rdunavail_type, 
	rdunavail.messagetype0 as rdunavail_message, 
	rdservice.lineid0 as LineID, 
	rdservice.subscriptionid0 as SubID,
	device.serialnumber0 as device_sn,
	device.baseconfigurationversion as device_gk,
	fw_version.firmwareversion as fw_version,
	concat(device.vendor0, " ", device.cliidentifier0) as device,
	ip_address.ip as ip,
	ssh_pass.password as telekom_password,
	line.uebertragungsprotokoll as phy_wan_proto, 
	line.physikalischesinterfacewan as phy_wan, 
	service.dienstebandbreiteup as upstream, 
	service.dienstebandbreitedown as downstream, 
	service.t4portaktiv as t4, 
	service.statischeprovisionierung as static_bng, 
	service.montageart as montageart, 
	service.stromversorgung as strom, 
	subscription.subscriptiontype0 as Service_Type, 
	subscription.subscriptionstatus0 as Sub_Status, 
	ports.portlocator as port_number, 
	ports.porttype as porttype,
	ports.description as port_description,
	ports.networkinterfaceid as ni_id, 
	ports.enableclock as SyncE, 
	ports.enableelmi as elmi, 
	businessport.physicallayer as physicallayer, 
	ports.ethertype as ethertype, 
	CASE 
		WHEN ports.bundlingtype = 'YES' THEN 'vlan' 
		when ports.bundlingtype = 'All-to-one' then 'port'  
		else null 
	END AS uni_bundling 
	from 
		netfactoryrd.rdservice_v02 as rdservice 
		left join netfactoryrd.subscription as subscription on subscription.subscriptionid0 = rdservice.subscriptionid0 
		left join netfactoryrd.rdprofile_v02 as rdprofile on rdprofile.subscriptionid0 = rdservice.subscriptionid0 
		left join netfactoryrd.port_conflet_v02 as ports on ports.parentuid LIKE rdprofile.uidacrosshistory 
		left join netfactoryrd.businessrdport_v03 as businessport on businessport.networkinterfaceid like ports.networkinterfaceid 
		left join netfactoryrd.rfs_business_rd_mgmt1_v03 as service on service.lineid = rdservice.lineid0 
		left join netfactoryrd.endgeraetefeatures_line1_v03 as line on line.parentuid = rdservice.rfsid0
		left join netfactoryrd.generalserviceattributes as service_attributes on service_attributes.uniqueid = rdservice.rfsid0

		#left join netfactoryrd.alarmstatus as alarm on alarm.rfsid0 = service_attributes.parentid and alarm.status0 != 'cleared'
		#alarms
		left join netfactoryrd.alarmstatus as rdauthfail on rdauthfail.rfsid0 = service_attributes.parentid and rdauthfail.status0 != 'cleared' and rdauthfail.eventtype = "rdauthfail"
	    left join netfactoryrd.alarmstatus as rdconfigfail on rdconfigfail.rfsid0 = service_attributes.parentid and rdconfigfail.status0 != 'cleared' and rdconfigfail.eventtype = "rdconfigfail"
	    left join netfactoryrd.alarmstatus as rdunavail on rdunavail.rfsid0 = service_attributes.parentid and rdunavail.status0 != 'cleared' and rdunavail.eventtype = "rdunavail"
		#device
		left join netfactoryrd.featureset as featureset on featureset.rfsid = rdservice.rfsid0
	    left join netfactoryrd.device as device on device.uniqueidentifier = featureset.deviceid 
	    left join netfactoryrd.firmwareversion as fw_version on fw_version.parentuid = featureset.deviceid
	    left join netfactoryrd.physicalport as phy_port on phy_port.physicalportmacaddress = device.macaddress
        left join netfactoryrd.sshuseraccount as ssh_pass on ssh_pass.unid = phy_port.parentuid and ssh_pass.username = 'telekom'
		#ip address
		left join netfactoryrd.view_device_leaseinfo as ip_address on ip_address.dhcpmacaddress = device.macaddress or ip_address.dhcpmacaddress = device.macaddress0
	where ((rdservice.subscriptionid0 like @subscription_id) or (rdservice.lineid0 like @subscription_id)) and rdprofile.status0 != 'deactivated'; 
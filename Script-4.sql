SELECT
 beb.subscriptionid,
 beb.evcid,
 beb.stag, 
 gsa.status
FROM netfactoryrd.businessethernetbridge_v03 as beb
inner join ( SELECT
 subscriptionid,
 evcid,
 stag,
 COUNT(stag) as stag_value 
FROM netfactoryrd.businessethernetbridge_v03
where nirfsid LIKE '1599483646553-325'
group by stag
having (stag_value > 1)) b
on beb.stag = b.stag
left join netfactoryrd.generalserviceattributes as gsa on gsa.uniqueid = beb.unid
order by beb.stag
set @bng = "ZSBJ03";
set @cluster = "Cluster2-AV"; 

select 
 *
from 
 dhcp.sharednetwork as network
 left join dhcp.standort as st on st.standortID = network.standortID 
 left join dhcp.standortgruppe as sg on (sg.standortGruppeID = st.standortGruppeID and sg.name = @cluster)
 left join dhcp.subnet as sn on sn.sharedNetworkID = network.sharedNetworkID 
 left join dhcp.pool as pool on pool.subnetID = sn.subnetID 
 left join netfactoryrd.view_device_leaseinfo as lease on ( inet_aton( lease.ip) between inet_aton(pool.rangeStart) and inet_aton(pool.rangeStop))
where 
 network.sharedNetwork = @bng
 
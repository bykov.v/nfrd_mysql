
select
 units.name as device,
 units2.name as console_server,
 unit_mapping.user_port as console_port,
 concat('Bykov.Viktor:',units2.name,':',unit_mapping.user_port) as username
from 
 avocentdatabase.units as units
 left join avocentdatabase.unit_connectivity_mappings as unit_mapping on unit_mapping.target_side_unit_id = units.oid 
 left join avocentdatabase.units as units2 on units2.oid = unit_mapping.user_side_unit_id

where (units.name like '%dtran%' or units.name like '%lcon%' or units.name like '%uawe%') and units2.name like '%TS%'
order by device
select 
 lease.ip as ip,
 lease.dhcpmacaddress as dhcpmacaddress,
 lease.dhcpclusternode as dhcp_server,
 lease.serialnumber as sn,
 lease.startupacs as acs,
 concat(device.vendor0, " ", device.cliidentifier0) as device,
 device.serialnumber0,
 fw_version.firmwareversion as fw_version,
 device.baseconfigurationversion as device_gk,
 ssh_pass.password as telekom_password
from
  netfactoryrd.view_device_leaseinfo as lease
  left join netfactoryrd.device as device on (device.macaddress = lease.dhcpmacaddress) or (device.macaddress0 = lease.dhcpmacaddress)
  left join netfactoryrd.firmwareversion as fw_version on fw_version.parentuid = device.id
  left join netfactoryrd.physicalport as phy_port on phy_port.physicalportmacaddress = device.macaddress
  left join netfactoryrd.sshuseraccount as ssh_pass on ssh_pass.unid = phy_port.parentuid and ssh_pass.username = 'telekom'
where 
  INET_ATON(lease.ip) BETWEEN INET_ATON("172.22.28.163") and INET_ATON("172.22.28.190")
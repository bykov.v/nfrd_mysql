#set @param = 'DEU.DTAG.HWTOP';
set @param = 'DEU.DTAG.HWTYP';

select
    rdservice.lineid0 as LineID,
    rdservice.status as service_status,
    gsa.parentid,
    gsa.servicename,
    gsa.uniqueid,
    rdservice.rdservicetype as service,
    rdservice.subscriptionid0 as SubID,
    rdservice.status as service_status,
    
    case when 
     (rdauthfail.status0 is null) and (rdconfigfail.status0 is null) and (rdunavail.status0 is null)
     then null 
     else concat(ifnull(rdauthfail.eventtype0,'-'), "/", ifnull(rdconfigfail.eventtype0, '-'), "/", ifnull(rdunavail.eventtype0,'-')) end 
    AS alarms, 

    device.serialnumber0 as device_sn,
    device.baseconfigurationversion as device_gk,
    fw_version.firmwareversion as fw_version,
    concat(device.vendor0, " ", device.cliidentifier0) as device,
    
    ip_address.ip as ip,
    ssh_pass.password as telekom_password,
    
    line.uebertragungsprotokoll as phy_wan_proto,
    line.physikalischesinterfacewan as phy_wan,
    
    service.dienstebandbreiteup as upstream,
    service.dienstebandbreitedown as downstream,
    service.t4portaktiv as t4,
    service.statischeprovisionierung as static_bng,
    service.montageart as montageart,
    service.stromversorgung as strom,
    
    ports.networkinterfacetype as porttype,
    ports.portnumber as port_number,
    ports.networkinterfaceid as ni_id,
    ports.synchronousmode as SyncE,
    ports.elmienable as elmi,
    ports.physicallayer as physicallayer,
    ports.ethertype as ethertype,
    ports.bundlingtype as bundling,
    ports.servicemultiplexing as servicemultiplexing,
    
    case
      when beb.evc_num > 0 then "EVC"
      when big.ipc_num > 0 then "IPC"
     END
    as service_name,
     case
      when beb.evc_num > 0 then beb.evc_num
      when big.ipc_num > 0 then big.ipc_num
     END
    as service_count
from netfactoryrd.rdservice_v02 as rdservice
join netfactoryrd.generalserviceattributes as gsa on rdservice.rfsid0 = gsa.parentid and gsa.servicename like '%Business RD Port%'
left join (select nirfsid, count(nirfsid) as evc_num from netfactoryrd.businessethernetbridge_v03 group by nirfsid) as beb on gsa.uniqueid = beb.nirfsid
left join (select nirfsid, count(nirfsid) as ipc_num from netfactoryrd.rfs_business_ip_gateway1_v03 group by nirfsid) as big
    on gsa.uniqueid = big.nirfsid

left join netfactoryrd.businessrdport_v03 as ports on ports.parentuid = gsa.uniqueid 
left join netfactoryrd.rfs_business_rd_mgmt1_v03 as service on service.lineid = rdservice.lineid0
left join netfactoryrd.endgeraetefeatures_line1_v03 as line on line.parentuid = rdservice.rfsid0

#alarms
left join netfactoryrd.alarmstatus as rdauthfail on rdauthfail.rfsid0 = gsa.parentid and rdauthfail.status0 != 'cleared' and rdauthfail.eventtype = "rdauthfail"
left join netfactoryrd.alarmstatus as rdconfigfail on rdconfigfail.rfsid0 = gsa.parentid and rdconfigfail.status0 != 'cleared' and rdconfigfail.eventtype = "rdconfigfail"
left join netfactoryrd.alarmstatus as rdunavail on rdunavail.rfsid0 = gsa.parentid and rdunavail.status0 != 'cleared' and rdunavail.eventtype = "rdunavail"
#device
left join netfactoryrd.featureset as featureset on featureset.rfsid = rdservice.rfsid0
left join netfactoryrd.device as device on device.uniqueidentifier = featureset.deviceid
left join netfactoryrd.firmwareversion as fw_version on fw_version.parentuid = featureset.deviceid
left join netfactoryrd.physicalport as phy_port on phy_port.physicalportmacaddress = device.macaddress
left join netfactoryrd.sshuseraccount as ssh_pass on ssh_pass.unid = phy_port.parentuid and ssh_pass.username = 'telekom'
#ip address
left join netfactoryrd.view_device_leaseinfo as ip_address on ip_address.dhcpmacaddress = device.macaddress or ip_address.dhcpmacaddress = device.macaddress0
where rdservice.lineid0 like @param or rdservice.subscriptionid0 like @param
group by uniqueid;
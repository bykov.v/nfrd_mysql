#set @subscription_id = '000000001443758';
#set @subscription_id = '822339001390841';
set @subscription_id = '000000001456007';


select 
	bb.subscriptionid as subsctiption_id, 
	service_attributes.parentid,
	service_attributes.status as evc_status,
	case when (rdauthfail.status0 is null) and (rdconfigfail.status0 is null) and (rdunavail.status0 is null)
 	  then null 
	  else concat(ifnull(rdauthfail.eventtype0,'-'), "/", ifnull(rdconfigfail.eventtype0, '-'), "/", ifnull(rdunavail.eventtype0,'-')) END
	AS alarms,
	#rdauthfail.status0 as rdauthfail_status, 
	#rdauthfail.eventtype0 as rdauthfail_type, 
	#rdauthfail.messagetype0 as rdauthfail_message, 
	#rdconfigfail.status0 as rdconfigfail_status, 
	#rdconfigfail.eventtype0 as rdconfigfail_type, 
	#rdconfigfail.messagetype0 as rdconfigfail_message, 
	#rdunavail.status0 as rdunavail_status, 
	#rdunavail.eventtype0 as rdunavail_type, 
	#rdunavail.messagetype0 as rdunavail_message, 
	bb.nirfsid as ni_id, 
	service_attributes.parentid as service_rfs_id, 
	bb.bandwidthingress as evc_speed, 
	bb.qosprofilel2 as evc_qos, 
	subs.subscriptionstatus0 as sub_status, 
	bb.evcid as evc_id, 
	bb.ttag as t_tag, 
	CASE WHEN bb.stag = 0 THEN NULL ELSE bb.stag END AS s_tag, 
	CASE WHEN vlanrange.tagstart = 0 THEN NULL ELSE vlanrange.tagstart END AS ce_vlan_start, 
	CASE WHEN vlanrange.tagend = 0 THEN NULL ELSE vlanrange.tagend END AS ce_vlan_end, 
	rdservice.lineid0 as line_id, 
	ba.dienstebandbreitedown as ba_speed_dn, 
	ba.dienstebandbreiteup as ba_speed_up, 
	CASE WHEN ba.t4portaktiv = 1 THEN 'enabled' ELSE 'disabled' END AS t4, 
	device.serialnumber0 as device_sn, 
	ssh_pass.password as telekom_password, 
	bport.networkinterfaceid as uni_id, 
	bport.networkinterfacetype as uni_type, 
	bport.bundlingtype as bundling, 
	CASE WHEN bport.bundlingtype = 'Yes' THEN 'vlan' ELSE 'port' END AS uni_bundling, 
	bport.synchronousmode as uni_sync_e, 
	bport.ethertype as uni_ethertype, 
	bport.elmienable as uni_elmi ,
	additional.type as evc_loop,
	additional.description as evc_loop_description
from 
	netfactoryrd.businessethernetbridge_v03 as bb 
	left join netfactoryrd.vlantagrange_v03 as vlanrange on vlanrange.parentuid = bb.parentuid 
	left join netfactoryrd.businessrdport_v03 as bport on bport.unid=bb.nirfsid 
	left join netfactoryrd.subscription as subs on subs.subscriptionid0=bb.subscriptionid 
	left join netfactoryrd.generalserviceattributes as service_attributes on service_attributes.uniqueid = bb.rfsid 
	left join netfactoryrd.rdservice_v02 as rdservice on rdservice.rfsid0 = service_attributes.parentid 
	left join netfactoryrd.featureset as featureset on featureset.rfsid = service_attributes.parentid 
	left join netfactoryrd.device as device on device.id = featureset.deviceid 
	left join netfactoryrd.alarmstatus as rdauthfail on rdauthfail.objectidentifier = service_attributes.parentid and rdauthfail.status0 != 'cleared' and rdauthfail.eventtype0 = "rdauthfail"
	left join netfactoryrd.alarmstatus as rdconfigfail on rdconfigfail.objectidentifier = service_attributes.parentid and rdconfigfail.status0 != 'cleared' and rdconfigfail.eventtype0 = "rdconfigfail"
	left join netfactoryrd.alarmstatus as rdunavail on rdunavail.objectidentifier = service_attributes.parentid and rdunavail.status0 != 'cleared' and rdunavail.eventtype0 = "rdunavail"
	left join netfactoryrd.rfs_business_rd_mgmt1_v03 as ba on ba.lineid = rdservice.lineid0 
	left join netfactoryrd.physicalport as phy_port on phy_port.physicalportmacaddress = device.macaddress 
	left join netfactoryrd.sshuseraccount as ssh_pass on ssh_pass.unid = phy_port.parentuid and ssh_pass.username = 'telekom'
	left join netfactoryrd.additionalserviceinformation as additional on additional.parentrfsid = service_attributes.parentid 
where bb.subscriptionid IN(@subscription_id) ;